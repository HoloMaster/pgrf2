package renderer;

import model.Element;
import model.enums.ElementType;
import model.enums.ObjectType;
import model.parts.Vertex;
import transforms.Mat4;
import transforms.Mat4Identity;
import transforms.Mat4PerspRH;
import transforms.Vec3D;
import view.Raster;

import java.awt.*;
import java.util.List;
import java.util.Optional;

import static utils.ColorPicker.colorPicker;

public class Renderer3D {

    private final Raster raster;
    Mat4 model, view, projection;
    private double[][] zbuffer;



    public Renderer3D(Raster raster) {
        this.raster = raster;
        model = new Mat4Identity();
        view = new Mat4Identity();

        projection = new Mat4PerspRH(Math.PI / 4, Raster.HEIGHT / (float) Raster.WIDTH, 1, 200);
        zbuffer = new double[Raster.WIDTH][Raster.HEIGHT];
    }

    private void clear() {
        raster.clear();
        for (int i = 0; i < zbuffer.length; i++) {
            for (int j = 0; j < zbuffer[i].length; j++) {
                zbuffer[i][j] = 1;
            }
        }
    }

    Vec3D transformToWindow(Vec3D v) {
        return v.mul(new Vec3D(1, -1, 1)) // Y jde nahoru, chceme dolu
                .add(new Vec3D(1, 1, 0)) // (0,0) je uprostřed, chceme v rohu
                // máme <0, 2> -> vynásobíme polovinou velikosti plátna
                .mul(new Vec3D(Raster.WIDTH / 2f, Raster.HEIGHT / 2f, 1));
    }

    public Mat4 getModel() {
        return model;
    }

    public void setModel(Mat4 model) {
        this.model = model;
    }

    public Mat4 getView() {
        return view;
    }

    public void setView(Mat4 view) {
        this.view = view;
    }

    public Mat4 getProjection() {
        return projection;
    }

    public void setProjection(Mat4 projection) {
        this.projection = projection;
    }

    public void draw(List<Element> elements, List<Vertex> vertices, List<Integer> indices, int switchero) {
        clear();
        for (Element element : elements) {


            if (element.getElementType() == ElementType.TRIANGLE) {
                for (int i = element.getStart(); i < element.getStart() + element.getCount() * 3; i += 3) {
                    if (element.getObjectType() == ObjectType.AXIS) {
                        Vertex a = vertices.get(indices.get(i));
                        Vertex b = vertices.get(indices.get(i + 1));
                        Vertex c = vertices.get(indices.get(i + 2));
                        prepareTriangle(a, b, c, switchero, ObjectType.AXIS);
                    } else {

                        Vertex a = vertices.get(indices.get(i));
                        Vertex b = vertices.get(indices.get(i + 1));
                        Vertex c = vertices.get(indices.get(i + 2));
                        prepareTriangle(a, b, c, switchero, ObjectType.CUBE);
                    }
                }
            } else if (element.getElementType() == ElementType.LINE) {

/*
                for (int i = element.getStart(); i < element.getStart() + element.getCount(); i += 2) {
                    Vertex a = vertices.get(indices.get(i));
                    Vertex b = vertices.get(indices.get(i + 1));
                    rasterize(a, b, a.color, b.color);


                }*/

            }

            raster.repaint();
        }
        }



    private void prepareTriangle(Vertex a, Vertex b, Vertex c,int switchero, ObjectType objectType) {
        if (ObjectType.AXIS == objectType){
            a = a.transformAxis(view,projection);
            b = b.transformAxis(view,projection);
            c = c.transformAxis(view,projection);
        }else{
            a = a.transform(model, view, projection);
            b = b.transform(model, view, projection);
            c = c.transform(model, view, projection);
        }
        if (a.x > a.w && b.x > b.w && c.x > c.w || a.x < -a.w && b.x < -b.w && c.x < -c.w) return;
        if (a.y > a.w && b.y > b.w && c.y > c.w || a.y < -a.w && b.y < -b.w && c.y < -c.w) return;
        if (a.z> a.w && b.z > b.w && c.z > c.w || a.z < 0 && b.z < 0 && c.z < 0) return;
        if (a.z < b.z) {
            Vertex x = a;
            a = b;
            b = x;
        }
        if (b.z < c.z) {
            Vertex x = b;
            b = c;
            c = x;
        }
        if (a.z< b.z) {
            Vertex x = a;
            a = b;
            b = x;
        }

        if (a.z< 0)return;

        if (b.z < 0 ){
            Vertex bb = clipping(a,b) ;
            Vertex cc = clipping(a,c);
            drawTriangle(a,bb,cc,switchero);

        }else if (c.z < 0){

            Vertex cc = clipping(b,c);
            Vertex ccc = clipping(a,c);
            drawTriangle(a,cc,ccc,switchero);

        }else drawTriangle(a,b,c,switchero);
    }

    private Vertex clipping (Vertex vertex, Vertex vertex1){
        double t = vertex.z/ (vertex.z- vertex1.z) ;
        return new Vertex(vertex.vertex.mul(1-t).add(vertex1.vertex.mul(t)),vertex.color);

    }

    private void drawTriangle(Vertex a, Vertex b, Vertex c, int switchero) {
        Color colorA = a.color;
        Color colorB = b.color;
        Color colorC = c.color;


        Optional<Vec3D> v1 = a.vertex.dehomog();
        Optional<Vec3D> v2 = b.vertex.dehomog();
        Optional<Vec3D> v3 = c.vertex.dehomog();

        //zahodi vrcholy kdyz se rovnaji 0

        if (!v1.isPresent() || !v2.isPresent() || !v3.isPresent()) return;

        Vec3D vec3D1 = v1.get();
        Vec3D vec3D2 = v2.get();
        Vec3D vec3D3 = v3.get();

        vec3D1 = transformToWindow(vec3D1);
        vec3D2 = transformToWindow(vec3D2);
        vec3D3 = transformToWindow(vec3D3);


        if (switchero == 1){
            rasterizeWire(vec3D1,vec3D2,vec3D3,switchero);
        }else {

            if (vec3D1.getY() > vec3D2.getY()) {
                Vec3D temp = vec3D1;
                vec3D1 = vec3D2;
                vec3D2 = temp;

                Color tempC = colorA;
                colorA = colorB;
                colorB = tempC;

            }
            if (vec3D2.getY() > vec3D3.getY()) {
                Vec3D temp = vec3D2;
                vec3D2 = vec3D3;
                vec3D3 = temp;

                Color tempC = colorB;
                colorB = colorC;
                colorC = tempC;
            }
            if (vec3D1.getY() > vec3D2.getY()) {
                Vec3D temp = vec3D1;
                vec3D1 = vec3D2;
                vec3D2 = temp;

                Color tempC = colorA;
                colorA = colorB;
                colorB = tempC;
            }


            for (int y = Math.max((int) vec3D1.getY() + 1, 0); y <= Math.min(vec3D2.getY(), Raster.HEIGHT - 1); y++) {

                double t1 = (y - vec3D1.getY()) / (vec3D2.getY() - vec3D1.getY());
                double x1 = (1 - t1) * vec3D1.getX() + t1 * vec3D2.getX();
                double z1 = (1 - t1) * vec3D1.getZ() + t1 * vec3D2.getZ();

                double t2 = (y - vec3D1.getY()) / (vec3D3.getY() - vec3D1.getY());
                double x2 = (1 - t2) * vec3D1.getX() + t2 * vec3D3.getX();
                double z2 = (1 - t2) * vec3D1.getZ() + t2 * vec3D3.getZ();

                // vyplnit barvy

                Color color12 = colorPicker(colorA,colorB,t1);
                Color color13 = colorPicker(colorA,colorC,t2);
                fillLine(y,x1,z1,x2,z2,color12,color13);

            }

            for (int y = Math.max((int) vec3D2.getY() + 1, 0); y <= Math.min(vec3D3.getY(), Raster.HEIGHT - 1); y++) {
                double t1 = (y - vec3D3.getY()) / (vec3D2.getY() - vec3D3.getY());
                double x1 = (1 - t1) * vec3D3.getX() + t1 * vec3D2.getX();
                double z1 = (1 - t1) * vec3D3.getZ() + t1 * vec3D2.getZ();

                double t2 = (y - vec3D3.getY()) / (vec3D1.getY() - vec3D3.getY());
                double x2 = (1 - t2) * vec3D3.getX() + t2 * vec3D1.getX();
                double z2 = (1 - t2) * vec3D3.getZ() + t2 * vec3D1.getZ();


                // vyplnit barvy
                Color color12 = colorPicker(colorC, colorB, t1);
                Color color13 = colorPicker(colorC, colorA, t2);
                fillLine(y, x1, z1, x2, z2, color12, color13);


            }
        }
    }
    protected void rasterizeWire(Vec3D v1, Vec3D v2, Vec3D v3,int switchero) {
        rasterize(v1, v2, switchero);
        rasterize(v1, v3, switchero);
        rasterize(v3, v2, switchero);
    }


    private void rasterize(Vec3D v1, Vec3D v2, int switchero) {
        raster.drawLine(v1.getX(), v1.getY(), v2.getX(), v2.getY(), Color.WHITE);
    }



    void fillLine(int y, double x1, double z1, double x2, double z2, Color colorA, Color colorB) {
        if (x1 > x2) {
            double temp = x1;
            x1 = x2;
            x2 = temp;

            temp = z1;
            z1 = z2;
            z2 = temp;

            Color tempC = colorA;
            colorA = colorB;
            colorB = tempC;
        }

        for (int x = (int) Math.max(x1 + 1, 0); x <= (int) Math.min(x2, Raster.WIDTH - 1); x++) {

            double tz = (x - x1) / (x2 - x1);
            double z = (1 - tz) * z1 + tz * z2;
            Color finalColor = colorPicker(colorA, colorB, tz);
            drawPixel(x,y,z,finalColor);

        }
    }
    private void drawPixel(int x , int y, double z , Color color) {

    if (z < zbuffer[x][y] && z >= 0) {
        zbuffer[x][y] = z;
        raster.drawPixel(x, y, color.getRGB());

    }

    }

}
