package utils;

import com.sun.istack.internal.NotNull;
import org.jetbrains.annotations.Contract;

import java.awt.*;
import java.util.Random;

public class ColorPicker {

    @Contract("_, _, _ -> new")
    public static Color colorPicker(Color c1, Color c2, double t){
        return new Color(
                Math.max(Math.min( (int) (c1.getRed()*(1-t) + c2.getRed()*t),255),0),
                Math.max(Math.min( (int) ( c1.getBlue()*(1-t) + c2.getBlue()*t),255),0),
                Math.max(Math.min( (int) (c1.getGreen()*(1-t) + c2.getGreen()*t),255),0
                ));
    }

    @NotNull
    public static Color createRandomColor(){
        int colorPos = 0;
        double goldenRatioConj = (1.0 + Math.sqrt(5.0)) / 2.0;
        float hue = new Random().nextFloat();

        hue += goldenRatioConj * (colorPos / (5 * Math.random()));
        hue = hue % 1;

        return Color.getHSBColor(hue, 0.5f, 0.95f);



    }
}
