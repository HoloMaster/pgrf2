package view;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

public class Raster extends JPanel {

    private final BufferedImage img;
    private final Graphics g;

    private static final int FPS = 1000 / 30;
    public static final int WIDTH = 800, HEIGHT = 600;

    public Raster() {
        setPreferredSize(new Dimension(WIDTH,HEIGHT));
        // inicializace image, nastavení rozměrů (nastavení typu - pro nás nedůležité)
        img = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        g = img.getGraphics();
        //setTimer();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, null);
       //setTimer();
        // pro zájemce - co dělá observer - https://stackoverflow.com/a/1684476
    }

    private void setTimer() {
        // časovač, který 30 krát za vteřinu obnoví obsah plátna aktuálním img
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                // říct plátnu, aby zobrazilo aktuální img
                repaint();
            }
        }, 0, FPS);
    }

    public void clear() {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, WIDTH, HEIGHT);
    }

    public void drawPixel(int x, int y, int color) {
        if (x < 0 || x > Raster.WIDTH-1 || y < 0 || y > Raster.HEIGHT-1 )return;
        img.setRGB(x, y, color);
    }

    public int getPixel(int x, int y) {
        return img.getRGB(x, y);
    }

    public void drawLine(double x1, double y1, double x2, double y2, Color color) {

               int x3 = (int) Math.round(x1);
               int y3 = (int) Math.round(y1);
               int x4 =(int) Math.round(x2);
               int y4 = (int) Math.round(y2);
               bresenhamnsAlgorithm(x3,y3,x4,y4,color);

    }

    private void bresenhamnsAlgorithm(int x1, int y1, int x2, int y2, Color color) {
        if ((x1 == x2) && (y1 == y2)) {
            drawPixel(x1, y1, color.getRGB());

        } else {
            int dx = Math.abs(x2 - x1);
            int dy = Math.abs(y2 - y1);
            int rozdil = dx - dy;

            int posun_x, posun_y;

            if (x1 < x2) posun_x = 1;
            else posun_x = -1;
            if (y1 < y2) posun_y = 1;
            else posun_y = -1;

            while ((x1 != x2) || (y1 != y2)) {

                int p = 2 * rozdil;

                if (p > -dy) {
                    rozdil = rozdil - dy;
                    x1 = x1 + posun_x;
                }
                if (p < dx) {
                    rozdil = rozdil + dx;
                    y1 = y1 + posun_y;
                }
                drawPixel(x1, y1, color.getRGB());
            }
        }
    }

}
