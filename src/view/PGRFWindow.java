package view;

import javax.swing.*;
import java.awt.*;

public class PGRFWindow extends JFrame {

    private final Raster raster;

    public PGRFWindow() {
        // bez tohoto nastavení se okno zavře, ale aplikace stále běží na pozadí
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("PGRF2 cvičení"); // titulek okna

        FlowLayout layout = new FlowLayout(FlowLayout.LEFT);
        layout.setHgap(0);
        layout.setVgap(0);

        setLayout(layout);

        raster = new Raster();
        raster.setFocusable(true);
        raster.grabFocus();
        add(raster); // vložit plátno do okna



        Label label = new Label();
        label.setText("Ovladani je prilozeno v souboru " +System.lineSeparator()+
                      "README.TXT  v korenove slozce. " +System.lineSeparator()+
                      "Zelená osa - osa Z, " +System.lineSeparator()+
                      "Modrá osa - osa Y, " +System.lineSeparator()+
                      "Červená osa - osa X ");
        label.setBackground(Color.WHITE);

        raster.add(label);

        pack();
        setLocationRelativeTo(null);// vycentrovat okno
    }

    public Raster getRaster() {
        return raster;
    }
}
