package controller;

import model.Element;
import model.ObjectBase;
import model.parts.Vertex;
import model.objects.*;
import renderer.Renderer3D;
import transforms.*;
import view.Raster;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class Controller3D {

    private Renderer3D renderer3D;
    private Camera camera;
    private int mx, my;
    private int switchingPosition = 1;
    private int switchingZoom = 0;

    private final Cube cube = new Cube(1);
    private final Pyramid pyramid = new Pyramid();
    private final Axis axis = new Axis();
    private final BicubicPlate bicubicPlate = new BicubicPlate();
    private final List<Element> elements = new ArrayList<>();
    private final List<Vertex> vertices = new ArrayList<>();
    private final List<Integer> indices = new ArrayList<>();
    private int switchero = 0;

    public Controller3D(Raster raster) {
        initObjects(raster);
        initListeners(raster);
    }

    private void initObjects(Raster raster) {
        renderer3D = new Renderer3D(raster);
        resetCamera();

          elements.add(cube.getElement());
          elements.add(pyramid.getElement());
          elements.add(axis.getElement());
          elements.add(bicubicPlate.getElement());
      //    elements.add(line.getElement());

        vertices.addAll(ObjectBase.getVertices());
        indices.addAll(ObjectBase.getIndices());

        renderer3D.draw(elements,vertices,indices,switchero);
    }

    private void resetCamera() {
        camera = new Camera(
                new Vec3D(0.5, -10, 5),
                Math.toRadians(90),
                Math.toRadians(-20),
                1, true
        );
        renderer3D.setView(camera.getViewMatrix());
    }


    private void initListeners(Raster raster) {
        raster.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                mx = e.getX();
                my = e.getY();
            }
        });

        raster.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                   double diff = (mx - e.getX()) / 100.0;
                   double azimuth = camera.getAzimuth() + diff;
                   camera = camera.withAzimuth(azimuth);
                   renderer3D.setView(camera.getViewMatrix());
                   renderer3D.draw(elements, vertices, indices,switchero);


                   double diffy = (my - e.getY()) / 100.0;
                   double zenit = camera.getZenith() + diffy;
                   if (zenit>Math.toRadians(90)) zenit = Math.toRadians(90);
                   if (zenit< Math.toRadians(-90)) zenit = Math.toRadians(-90);
                   camera = camera.withZenith(zenit);
                   renderer3D.setView(camera.getViewMatrix());
                    super.mouseDragged(e);

                    // dodělat zenit, ořezat <-PI/2,PI/2>




                }else if (SwingUtilities.isRightMouseButton(e)){
                    double rotX = (mx - e.getX()) / -200.0;
                    double rotY = (my - e.getY()) / -200.0;
                    Mat4 rot = renderer3D.getModel().mul(new Mat4RotXYZ(rotY, 0, rotX));
                    renderer3D.setModel(rot);
                    renderer3D.draw(elements,vertices,indices,switchero);
                }

                mx = e.getX();
                my = e.getY();
            }
        });

        raster.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_UP:
                    case KeyEvent.VK_W:
                        camera = camera.forward(0.1);
                       renderer3D.setView(camera.getViewMatrix());
                        renderer3D.draw(elements,vertices,indices,switchero);
                        break;

                    case KeyEvent.VK_DOWN:
                    case KeyEvent.VK_S:
                        camera = camera.backward(0.1);
                        renderer3D.setView(camera.getViewMatrix());
                        renderer3D.draw(elements,vertices,indices,switchero);
                        break;

                    case KeyEvent.VK_LEFT:
                    case KeyEvent.VK_A:
                        camera = camera.left(0.1);
                        renderer3D.setView(camera.getViewMatrix());
                        renderer3D.draw(elements,vertices,indices,switchero);

                        break;

                    case KeyEvent.VK_RIGHT:
                    case KeyEvent.VK_D:
                        camera = camera.right(0.1);
                        renderer3D.setView(camera.getViewMatrix());
                        renderer3D.draw(elements,vertices,indices,switchero);
                        break;

                    case KeyEvent.VK_P: renderer3D.setProjection(
                            new Mat4PerspRH(Math.PI / 4, Raster.HEIGHT / (float) Raster.WIDTH, 1, 200));
                        renderer3D.draw(elements,vertices,indices,switchero);
                    break;
                    case KeyEvent.VK_O: renderer3D.setProjection(
                            new Mat4OrthoRH(10,10,0.00001,200));
                        renderer3D.draw(elements,vertices,indices,switchero);
                    break;

                    case KeyEvent.VK_T:
                        Mat4 translate = renderer3D.getModel().mul(
                                new Mat4Transl(0*switchingPosition,0*switchingPosition,0.2*switchingPosition));
                        renderer3D.setModel(translate);
                        renderer3D.draw(elements,vertices,indices,switchero);
                        break;

                    case KeyEvent.VK_Z:
                        Mat4 scale = renderer3D.getModel().mul(
                                new Mat4Scale(0.6+switchingZoom,0.6+switchingZoom,0.6+switchingZoom));
                        renderer3D.setModel(scale);
                        renderer3D.draw(elements,vertices,indices,switchero);
                        break;
                    case KeyEvent.VK_G:
                        switchero = 1;
                        renderer3D.draw(elements,vertices,indices,switchero);
                        break;
                    case KeyEvent.VK_H:
                        switchero =0;
                        renderer3D.draw(elements,vertices,indices,switchero);
                        break;
                    case KeyEvent.VK_C:
                        if (switchingZoom == 0){
                            switchingZoom = 1;
                        }else if(switchingZoom == 1){
                            switchingZoom = 0;
                        }
                        switchingPosition = switchingPosition*(-1);

                        break;

                }
            }
        });
    }
}
