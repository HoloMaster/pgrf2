package model;

import model.enums.ElementType;
import model.enums.ObjectType;

public class Element {

    private final ElementType elementType;
    private final ObjectType objectType;
    private final int start;
    private final int count;
    private final int verticiesCount;

    public ElementType getElementType() {
        return elementType;
    }

    public ObjectType getObjectType() {
        return objectType;
    }

    public int getStart() {
        return start;
    }

    public int getCount() {
        return count;
    }

    public int getVerticiesCount() {
        return verticiesCount;
    }

    public Element(ElementType elementType, ObjectType objectType, int start, int count, int verticiesCount) {
        this.elementType = elementType;
        this.objectType = objectType;
        this.start = start;
        this.count = count;
        this.verticiesCount = verticiesCount;
    }
}
