package model;


import model.interfaces.Objects;
import model.parts.Vertex;

import java.util.Arrays;
import java.util.List;

public abstract class ObjectBase implements Objects {

    public ObjectBase() {
    }

    @Override
    public void addIndices(Integer... in) {
        indices.addAll(Arrays.asList(in));
    }

    public static List<Vertex> getVertices() {
        return vertices;
    }


    public static List<Integer> getIndices() {
        return indices;
    }

    @Override
    public Integer getCountOfVerticies() {
        return vertices.size();
    }


}
