package model.parts;

import transforms.Mat4;
import transforms.Point3D;

import java.awt.*;

public class Vertex {

    public final Point3D vertex;
    public final Color color;
    public final double x, y, z, w;

    public Vertex(Point3D vertex, Color color) {
        this.vertex = vertex;
        this.color = color;
        x = vertex.getX();
        y = vertex.getY();
        z = vertex.getZ();
        w = vertex.getW();
    }
    public Vertex transform(Mat4 model, Mat4 view, Mat4 projection){
        return new Vertex(vertex.mul(model).mul(view).mul(projection),color);
    }

    public Vertex transformAxis(Mat4 view, Mat4 projection){
        return new Vertex(vertex.mul(view.mul(projection)),color);
    }


}
