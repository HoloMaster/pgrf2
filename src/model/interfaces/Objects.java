package model.interfaces;



import model.parts.Vertex;

import java.util.ArrayList;

import java.util.List;

public interface Objects {

     List<Vertex> vertices = new ArrayList<>();
     List<Integer> indices = new ArrayList<>();

     void addIndices(Integer... in);

     Integer getCountOfVerticies();
}
