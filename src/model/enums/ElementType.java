package model.enums;

public enum ElementType {
    POINT, LINE, TRIANGLE
}
