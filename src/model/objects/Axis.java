package model.objects;

import model.*;
import model.enums.ElementType;
import model.enums.ObjectType;
import model.parts.Vertex;
import transforms.Point3D;

import java.awt.*;


public class Axis extends ObjectBase {

    private final Element element;

    public Axis() {
        int lastIndexV = getCountOfVerticies() ;
        int lastIndexI = indices.size();
        double x = 0.035;



        vertices.add(new Vertex(new Point3D(x,0,0), Color.green));  //0
        vertices.add(new Vertex(new Point3D(-x,0,0), Color.green)); //1
        vertices.add(new Vertex(new Point3D(-x,0,3), Color.green)); //2
        vertices.add(new Vertex(new Point3D(x,0,3), Color.green));  //3
        vertices.add(new Vertex(new Point3D(-1,0,3),Color.green));
        vertices.add(new Vertex(new Point3D(1,0,3),Color.green));
        vertices.add(new Vertex(new Point3D(0,0,4),Color.green));     //4

        vertices.add(new Vertex(new Point3D(-x,0,x), Color.red));
        vertices.add(new Vertex(new Point3D(-x,0,-x), Color.red));
        vertices.add(new Vertex(new Point3D(3,0,x), Color.red));
        vertices.add(new Vertex(new Point3D(3,0,-x), Color.red));
        vertices.add(new Vertex(new Point3D(3,0,1),Color.red));
        vertices.add(new Vertex(new Point3D(3,0,-1),Color.red));
        vertices.add(new Vertex(new Point3D(4,0,0),Color.red));

        vertices.add(new Vertex(new Point3D(x,0,0), Color.blue));
        vertices.add(new Vertex(new Point3D(-x,0,0), Color.blue));
        vertices.add(new Vertex(new Point3D(x,3,0), Color.blue));
        vertices.add(new Vertex(new Point3D(-x,3,0), Color.blue));
        vertices.add(new Vertex(new Point3D(-1,3,0),Color.blue));
        vertices.add(new Vertex(new Point3D(1,3,0),Color.blue));
        vertices.add(new Vertex(new Point3D(0,4,0),Color.blue));

        addIndices(lastIndexV,lastIndexV + 1,lastIndexV + 2);
        addIndices(lastIndexV + 2,lastIndexV,lastIndexV + 3);
        addIndices(lastIndexV + 3,lastIndexV + 2,lastIndexV + 4);
        addIndices(lastIndexV + 4,lastIndexV + 5,lastIndexV + 6);
        addIndices(lastIndexV+6,lastIndexV+3,lastIndexV+2);

        addIndices(lastIndexV + 7,lastIndexV + 8,lastIndexV + 10);
        addIndices(lastIndexV + 10,lastIndexV + 7,lastIndexV + 9);
        addIndices(lastIndexV + 9,lastIndexV + 10,lastIndexV + 11);
        addIndices(lastIndexV + 11,lastIndexV + 12,lastIndexV + 13);
        addIndices(lastIndexV +13 ,lastIndexV +9, lastIndexV +10);

        addIndices(lastIndexV + 14,lastIndexV + 15,lastIndexV + 17);
        addIndices(lastIndexV + 17,lastIndexV + 14,lastIndexV + 16);
        addIndices(lastIndexV + 16,lastIndexV + 17,lastIndexV + 18);
        addIndices(lastIndexV + 18,lastIndexV + 19,lastIndexV + 20);
        addIndices(lastIndexV+20,lastIndexV+16,lastIndexV+17);

        element = new Element(ElementType.TRIANGLE, ObjectType.AXIS, lastIndexI,15, vertices.size());
    }


    public Element getElement(){
        return element;
    }


}
