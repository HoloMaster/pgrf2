package model.objects;


import model.*;
import model.enums.ElementType;
import model.enums.ObjectType;
import model.parts.Vertex;
import transforms.Point3D;

import java.awt.*;

public class Line extends ObjectBase {
    private final Element element;

    public Line() {
        int lastIndexV = getCountOfVerticies();
        int lastIndexI = indices.size();

        vertices.add(new Vertex(new Point3D(0, 0, 0), Color.green));
        vertices.add(new Vertex(new Point3D(0, 0, 5), Color.green));



        addIndices(lastIndexV, lastIndexV + 1);


        element = new Element(ElementType.LINE, ObjectType.LINE, lastIndexI, 1, vertices.size());

    }
    public Element getElement(){
        return element;
    }











}
