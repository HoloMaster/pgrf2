package model.objects;

import model.*;
import model.enums.ElementType;
import model.enums.ObjectType;
import model.parts.Vertex;
import transforms.Bicubic;
import transforms.Cubic;
import transforms.Point3D;

import java.awt.*;

import static utils.ColorPicker.createRandomColor;

public class BicubicPlate extends ObjectBase {

    private Element element;

    public BicubicPlate() {
        int lastIndexV = getCountOfVerticies() ;
        int lastIndexI = indices.size();
        int hladkost = 16;
        double x =1.5;


        Point3D points[] = new Point3D[]{
                new Point3D(-3*x, 2*x, 2*x),
                new Point3D(-1*x, 0*x, 2*x),
                new Point3D(3*x, 4*x, 0*x),
                new Point3D(5*x, 0*x, 0*x),
                new Point3D(-4*x, -0.4*x, 2*x),
                new Point3D(-1*x, 1*x, 2*x),
                new Point3D(3*x, 0.5*x, 2*x),
                new Point3D(5*x, 0*x, 2*x),
                new Point3D(-4*x, 0*x, 3*x),
                new Point3D(-1*x, 0*x, 3*x),
                new Point3D(3*x, 0.5*x, 3*x),
                new Point3D(3*x, 0*x, 3*x),
                new Point3D(-4*x, 5*x, 5*x),
                new Point3D(-1*x, 6*x, 4*x),
                new Point3D(3*x, 5*x, 1*x),
                new Point3D(6*x, 5*x, 2*x),
        };

        Bicubic cubic = new Bicubic(Cubic.BEZIER, points);

        Color color = new Color(0);

        for (int i = 0; i <= hladkost; i++) {
            for (int j = 0; j <= hladkost; j++) {
                if(i%20 == 0 || j % 20 == 0){
                    color = createRandomColor();
                }
                vertices.add(new Vertex(cubic.compute((double)i / hladkost,
                        (double) j / hladkost), color));
            }
        }
        for (int i = 0; i <= hladkost - 1; i++) {
            for (int j = 0; j <= hladkost - 1; j++) {
                addIndices((i*(hladkost+1)+j)+ lastIndexV,
                            (i*(hladkost+1)+j+1)+ lastIndexV,
                             ((i + 1) * (hladkost + 1) + j)+ lastIndexV);

                addIndices((i*(hladkost+1)+j+1)+ lastIndexV,
                            ((i+1)*(hladkost+1)+j)+ lastIndexV,
                            ((i + 1) * (hladkost + 1) + j+1)+ lastIndexV);
            }
        }
        element = new Element(ElementType.TRIANGLE, ObjectType.BICUBIC,lastIndexI,448, getCountOfVerticies());

    }

    public Element getElement(){
        return element;
    }



}
