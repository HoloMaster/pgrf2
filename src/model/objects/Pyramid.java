package model.objects;

import model.*;
import model.enums.ElementType;
import model.enums.ObjectType;
import model.parts.Vertex;
import transforms.Point3D;

import static utils.ColorPicker.createRandomColor;


public class Pyramid extends ObjectBase {

    private final Element element;

    public Pyramid() {
        int lastIndexV = getCountOfVerticies() ;
        int lastIndexI = indices.size();

        vertices.add(new Vertex(new Point3D( -1.5, -1.5, 1), createRandomColor()));
        vertices.add(new Vertex(new Point3D( 1.5, -1.5, 1), createRandomColor()));
        vertices.add(new Vertex(new Point3D( 1.5, 1.5, -1), createRandomColor()));
        vertices.add(new Vertex(new Point3D( -1.5, 1.5, -1), createRandomColor()));
        vertices.add(new Vertex(new Point3D( 0, 0, 1.5), createRandomColor()));



        addIndices(lastIndexV,lastIndexV+1,lastIndexV+2);
        addIndices(lastIndexV,lastIndexV + 2,lastIndexV + 3);
        addIndices(lastIndexV,lastIndexV + 3,lastIndexV + 4);
        addIndices(lastIndexV,lastIndexV+ 4,lastIndexV + 1);
        addIndices(lastIndexV + 1,lastIndexV + 4,lastIndexV + 2);
        addIndices(lastIndexV + 3,lastIndexV + 2,lastIndexV + 4);


        element = new Element(ElementType.TRIANGLE, ObjectType.PYRAMID, lastIndexI,6, vertices.size());

    }

    public Element getElement(){
        return element;
    }




}
