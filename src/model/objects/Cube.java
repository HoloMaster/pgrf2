package model.objects;

import model.*;
import model.enums.ElementType;
import model.enums.ObjectType;
import model.parts.Vertex;
import transforms.Point3D;

import static utils.ColorPicker.createRandomColor;

public class Cube extends ObjectBase {

    private final Element element;


    public Cube(int x) {
        vertices.add(new Vertex(new Point3D( -x, -x, -x), createRandomColor()));
        vertices.add(new Vertex(new Point3D( x, -x, -x), createRandomColor()));
        vertices.add(new Vertex(new Point3D(  x, x, -x), createRandomColor()));
        vertices.add(new Vertex(new Point3D( -x, x, -x), createRandomColor()));

        vertices.add(new Vertex(new Point3D( -x, -x, x), createRandomColor()));
        vertices.add(new Vertex(new Point3D( x, -x, x), createRandomColor()));
        vertices.add(new Vertex(new Point3D( x, x, x), createRandomColor()));
        vertices.add(new Vertex(new Point3D( -x, x, x), createRandomColor()));

        // spodní strana
        addIndices(0, 1, 2);
        addIndices(0, 2, 3);
        // přední strana
        addIndices(0, 1, 5);
        addIndices(0, 5, 4);
        // pravá strana
        addIndices(1, 2, 6);
        addIndices(1, 6, 5);
        //zadní strana
        addIndices(3, 2, 6);
        addIndices(3, 6, 7);
        // levá strana
        addIndices(3, 0, 4);
        addIndices(3, 4, 7);
        // horní strana
        addIndices(4, 5, 6);
        addIndices(4, 6, 7);

        element = new Element(ElementType.TRIANGLE, ObjectType.CUBE, 0,12, vertices.size());
    }


    public Element getElement(){
        return element;
    }


}
